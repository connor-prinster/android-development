# Android Notes ([sorry about the mess](https://bitbucket.org/connor-prinster/android-development/raw/68b059989e2f106015ef8c118f20c273dc8526e0/resources/sorry_about_the_mess.jpg)!):

---

---

[Preparing Your Workspace](#how-to-download-and-use-android-studio)

[Helpful Links](#links-to-helpful-sites)

[Different View Tags](#views)

---

---

## How to Download and Use Android Studio:

[Configure your project](#configure-your-project)

[Set up phone for debugging](#set-up-your-phone-for-debugging)

[Download Drivers](#download-drivers-(only-for-windows))

[Debugging on phone](#debugging-on-your-phone)

---


Go and download the [Android Studio](https://developer.android.com/studio/) appropriate for your machine

### Configure your project
* The application name will be on the top of the app
* The `Company domain` can be anything really, but the one I used for straight up practice was `android.example.com`.
* the package name will automatically be generated and it should look like `com.example.android.<project_name`>
* set the type of Android device you want to configure. Since I was doing phone development I checked the `Phone and Tablet` box. 
* You have to choose the minimum SDK for whatever you're programming for and since the minimum version for me would be IceCreamSandwich I chose that. It was also the first automatically filled in choice.
* You then have to choose an Activity and I chose `Empty Activity`.
* Customize the `Activity Name` and `Layout Name`. I don't know exactly what it is exactly, but my first values were `MainActivity` and `activity_main` respectively

### Set up your phone for debugging
* First you've got to have a phone and a usb cable for your phone
* You have to become a developer
    * Open up `settings` app
    * Go down to `about`
    * Tap `Build Number` seven times to set the phone to developer mode
        * There SHOULD be a popup message that says something along the lines of "Developer Mode Activated." I forgot to write it down or I would have.
    * To check for sure that the developer mode is on, there should be a `Developer options` menu thing in the `Settings` app.
    * After you've said that you're a developer and the menu is now visible, tap into it.
    * Scroll down until you can see the `USB debugging` radio button or checkbox and check/tap it and then tap `ok` on the popup

### Download drivers (only for Windows)
* Follow the [link](https://developer.android.com/studio/run/oem-usb#Drivers) to the download page or search for `OEM Android`, click on one from developer.android.com, and then follow the instructions on the page under the `Install a USB Driver` section.
* Look for the driver specific to your phone. I'm using a Samsung Galaxy S6+ Edge so I followed the link associated with Samsung
* You should eventually be able to download a .zip file. I put it in my Downloads folder, unzipped it, and opened the wizard. 
* Next, go to the `Computer Management` menu. I just searched for it on Cortana
* Go to `Device Manager -> Portable Devices` and click on the phone you've attached. 
* Once I got to that dropdown item I right-clicked on my phone which brough up another window. I was given two options: `Search automatically for updated driver software` and `Browse my computer for driver software` and I clicked on the former. The woman on Udacity said to search for it myself but I think that since I downloaded the Samsung driver to the default location the computer was able to search automatically well enough.
* follow whatever the wizard or whatever tells you to do
* When my download was done I got a popup window that said `The best drivers for your device are already installed`.
* Once all the above has been done you should be able to debug on your phone

### Debugging on your phone
* Hopefully you know at least a little xml. The project I created already had stuff for me written so running it wasn't too horrible. 
* The code written for me had stuff about ConstraintPanes but the Udacity lady said to not deal with that. and instead insert the code below (though I've edited it since then because her code wasn't ideal for my machine)
```xml
<?xml version="1.0" encoding="utf-8"?>
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    xmlns:tools="http://schemas.android.com/tools"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    tools:context="com.example.android.<project>.<class>">

    <TextView
        android:id="@+id/helloWorld"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        />
</RelativeLayout>
```
* I did get an issue with the `android:text="Hello World!"` section of code and it refused to run on my phone. In order to get it to work on the first time I would recommend going into your automatically generated `res/values/strings.xml` and inserting a new value called `<string name="hello_world">Hello, World!</string>`. Then, in the code above, change the faulty piece of code to `android:text="@string/happy_birthday"` instead. 
* Up in the top-right section of the toolbar (on a Windows device at least), there should be a green play button; press it. 
* For me the app didn't start automatically. I had to choose which phone I wanted the app to download to and I clicked on my Samsung S6. It took a while to download the first time and stuff said it was downloading on the bottom of Android Studio. 
* Anyways, after a while the Android Studio terminal should open up and stuff should start happening.
    * For me, if it wasn't going to run correctly I got red text in the terminal, if you get none you should be fine.
* Your phone should eventually pop up with `Hello, World!` on a white screen with a blue title!
* Now you can start doing more crazy awesome stuff.

---

---

## Views:

[TextView](#textview)

[ImageView](#imageview)

[View Groups](#view-groups)

### TextView:

 dp = density independent pixels. Regardless of the resolution of the screen the button or item will take up the same amount of real estate. Even though a button may only take up 2px by 2px on a low res screen it may take up 8px by 8px on a high resolution screen
    

 hardcoding a value is tpically a bad idea for designing apps for any Android device since they come in like a billion forms, better to set the value to **wrap_content**
```xml
    <TextView
    android:layout_width="300dp"
    android:layout_height="500dp" />

    <!--
    This is an example of hardcoding which isn't best practice. If you wanted it to wrap the text so it takes up no more space than is necessary, have it look like the below>
    -->

    <TextView
    android:layout_width="wrap_content"
    android:layout_height="wrap_content" />

    <!--
    Having the height or width as wrap_text does not mean the other has to be that way as well, the below and vice versa is completely valid. Similar to binding in JavaFX I think?
    -->
    <TextView
    android:layout_width="wrap_content"
    android:layout_height="non-wrap_content" />
```
Can change text font size with code snippet like
```xml
<TextView 
android:textSize="45sp" />
    <!--
    sp stands for scale-independent pixels which is just like dp and is used to make the app look consistent across other devices and varying densities
    sp is used only for fonts because the text will adjust based on the user's preference choices in the device's settings. It's a global value that changes every sp in the entire phone as far as I'm currently aware. 
    -->
```
In the Udacity video is appears that there are at **least** four sizes on Google devices: Small, Normal, Large, and Huge. You can apparently put this text into the XML like so
```xml
<TextView 
?android:textAppearanceSmall
?android:textAppearanceMedium 
?android:textAppearanceLarge />
<!-- 
the small is 14sp
the medium is 18sp
the large is 22sp
-->
```
The benefit of using the above code instead of custom text is that it will fit in smoothly with the other apps and stuff on a phone

 Another thing that you can change in the TextView is the colors as shown below

```xml
<!--- BACKGROUND EXAMPLES -->

<!--- The two following are valid -->
<TextView
    android:background="@android:color/darker_gray"/>
<TextView
    android:background="#2196F3"/>
<!--- below is NOT valid because the keyword blue isn't supported -->
<TextView
    android:background="@android:color/blue"/>
```
```xml
<!--- TEXT EXAMPLES -->
<TextView
    android:textColor="#AED581"/>
<TextView
    android:textColor="@android:color/darker_gray"/>
<!--- Still not supported -->
<TextView
    android:textColor="@android:color/blue"/>
```

---
### ImageView: 

Android XML also allows for pictures. Here is valid code
```xml
<!--- no need for file extension-->
<ImageView
    android:src="@drawable/cake"  
    android:layout_width="wrap_content"
    android:layout_height="wrap_content"
    android:scaleType="center"/>
    <!--- if the image is too anything the scaleType can be used to center, centerCrop, etc it. It's like how you can change how a background is seen on a desktop -->
```
increasing the width and height **dp** value actually increases the way the image thinks of itself, sorta? Like if a picture is too small for a screen but you set it to 900dp * 900dp and then center it, it will be in the center of the screen because it thinks it's big enough when it really isn't

---

### View Groups:

If you want multiple things on a screen, this is the way to do it. It's the root of an XML document. It's like an anchorpane in JavaFX
* children of a root are siblings
* remember that when talking about views, the parent of a bunch of children also count as a view.

```xml
<!--- An example of valid two-child code -->
<!--- The container is called a ViewGroup but isn't tagged as such -->
<!--- The below parent code is called an opening tag -->
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="wrap_content"
    android:layout_height="wrap_content">
    <!-- notice that there is no closing bracket above --->
 
    <TextView
        android:text="Guest List"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
 
    <TextView
        android:text="Party Guest #1"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content" />
 
 <!--- The below parent code is called an closing tag -->
 <!--- Notice that the closing bracket is before the LinearLayout finisher-->
</LinearLayout>
```

In the above code there is a line that reads
```xml
xmlns:android="http://schemas.android.com/apk/res/android"
```
It's a namespace. It's like how you'd use `std::` or `chrono::` in C++ to specify which library you're using.

### Height and Width on Child Views:

There is a new value called `match_parent` which applies only to height and width to my current knowledge. It causes the child node to match either the height or width of it's view group's parent. It won't match to its sibling nodes but it can help so no data gets cut off. 
```xml
<!--match_parent below will match the phone screen's values--->
<LinearLayout

    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="@android:color/darker_gray">

    <!--wrap text means that it'll go all the way across whatever screen you've got. match_parent here is somewhat useless tbh--->
    <!--width and height definitely don't have to match. One could be match_parent while the other one has nothing to do with the parent--->
    <TextView
        android:text="VIP List"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#4CAF50"
        android:textSize="24sp" />

    <TextView
        android:text="Kunal"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#4CAF50"
        android:textSize="24sp" />

    <TextView
        android:text="Kagure"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#4CAF50"
        android:textSize="24sp" />

    <TextView
        android:text="Lyla"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:background="#4CAF50"
        android:textSize="24sp" />

</LinearLayout>
```

---

---

## On-screen Organization

[Even Spacing](#even-spacing)

[Relative Layout](#relative-layout)

[Padding and Margin](#padding-and-margin)

#### Even Spacing:
The spacing is definitely noticed in portrait, landscape, and tablet views. Take advantage of the screen's remaining [free real estate](http://i0.kym-cdn.com/entries/icons/mobile/000/021/311/free.jpg). You'd use the keyword `android:layout_weight`. 
* The default value is 0. 
* The higher the weight value the more space it'll have between it and the preceding/succeeding one. 
* The weights will fill in the rest of the screen. If one has a weight of 2, it will give more of the remaining space to the child with the higher weight (in this case 2). 
* It adds all the weights together and then divides the screen on that. For example, if all the weights add up to 10 and the children have weights 1, 8, 1, the children will have 10%, 80%, and 10% of the available screen which makes a lot of sense. 
* Regardless of the `linear_layout` value the spacing will work just fine.
    * If the layout is horizontal, all the children will fill in the horizontal real estate and vertical real estate for vertical layout. Pretty simple really. 
    * Equal weights = equal heights for vertical and equal weights = equal widths in horizontal. 
    * If you are not sure the height of the values when in `vertical` setting, set the height to 0dp and then the weight to whatever and then it'll work itself out. The same goes to the `horizontal` setting except you'd set the width to 0dp. 
    * A good example of this is the email apps where the text input node has a much bigger space than to the `from`, `to`, and `subject` lines but you want everything to fit the screen
    * if only one child has a weight to it, it doesn't really matter what the weight is assigned. It will still take up the rest of the screen because it'll have 100% of the remaining real estate

Valid code below
```xml
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <!---Since there is only weight on the ImageView down below, any space not taken up by the TextViews will be filled with the photo-->
    <ImageView
        android:src="@drawable/ocean"
        android:layout_width="wrap_content"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:scaleType="centerCrop" />

    <TextView
        android:text="You're invited!"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textColor="@android:color/white"
        android:textSize="54sp"
        android:background="#009688" />

    <TextView
        android:text="Bonfire at the beach"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:textColor="@android:color/white"
        android:textSize="34sp"
        android:background="#009688" />
</LinearLayout>
```

### Relative Layout:
New instruction called `layout_alignParent*=<boolean>` is a thing. Options are:
* android:layout_alignParentTop
* android:layout_alignParentBottom
* android:layout_alignParentLeft
* android:layout_alignParentRight

You can have multiple things set to true, like top-right, bottom-left, etc.

Example shown below:
```xml
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:orientation="vertical"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:src="@drawable/ocean"
        android:layout_width="200dp"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:layout_alignParentTop="true"
        android:layout_alignParentLeft="true"
        android:scaleType="centerCrop" />
        
    <ImageView
        android:src="@drawable/rocks"
        android:layout_width="200dp"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:layout_alignParentBottom="true"
        android:layout_alignParentRight="true"
        android:scaleType="centerCrop" />
    
    <TextView
        android:text="You're invited!"
        android:layout_width="wrap_content"
        android:layout_height="0dp"
        android:layout_alignParentTop="true"
        android:layout_alignParentRight="true"
        android:textColor="@android:color/black"
        android:textSize="25sp"/>

    <TextView
        android:text="You're invited!"
        android:layout_width="wrap_content"
        android:layout_height="0dp"
        android:layout_alignParentBottom="true"
        android:layout_alignParentLeft="true"
        android:textColor="@android:color/black"
        android:textSize="25sp"/>
</RelativeLayout>
```
You can assign items to different places relative to other children. To do this, you have to assign a variable an id with `android:id="@id/*"`
* the @ refers to a resource in app
* id = type
* \+ is because you're delcaring it for the first time.
* \* refers to the name, it could be like `"i_am_a_valid_id"` 
    * the syntax for id's require that there are no spaces (hence the underscores), no odd characters, and must start with a letter

For the above, the valid code would look like `android:id="@+id/i_am_a_valid_id"`

To set it relative to another value, you can insert `android:layout_to*of="@id/i_am_a_valid_id` in the child's tag or whatever

Valid layout codes are:
* `layout_toRightOf`
* `layout_toLeftOf`
* `layout_above`
* `layout_below`

Below is a massive chunk of code, but is super helpful in viewing things. 
```xml
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <TextView
        android:id="@+id/lyla_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_alignParentLeft="true"
        android:textSize="24sp"
        android:text="Lyla" />

    <TextView
        android:id="@+id/me_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_toRightOf="@id/lyla_text_view"
        android:textSize="24sp"
        android:text="Me" />

    <TextView
        android:id="@+id/natalie_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_above="@id/lyla_text_view"
        android:textSize="24sp"
        android:text="Natalie" />

    <TextView
        android:id="@+id/jennie_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_alignParentRight="true"
        android:textSize="24sp"
        android:text="Jennie" />

    <TextView
        android:id="@+id/omoju_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentRight="true"
        android:layout_above="@id/jennie_text_view"
        android:textSize="24sp"
        android:text="Omoju" />

    <TextView
        android:id="@+id/amy_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentBottom="true"
        android:layout_alignParentRight="true"
        android:layout_above="@id/omoju_text_view"
        android:textSize="24sp"
        android:text="Amy" />

    <TextView
        android:id="@+id/ben_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_centerHorizontal="true"
        android:textSize="24sp"
        android:text="Ben" />

    <TextView
        android:id="@+id/kunal_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_toLeftOf="@id/ben_text_view"
        android:textSize="24sp"
        android:text="Kunal" />

    <TextView
        android:id="@+id/kagure_text_view"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:layout_alignParentTop="true"
        android:layout_toRightOf="@id/ben_text_view"
        android:textSize="24sp"
        android:text="Kagure" />

</RelativeLayout>
<!---
The Layout will look like:
Top: KunalBenKagure
Bottom Left: Natalie \n LylaMe
Bottom Right: Amy \n Omoju \n Jennie
-->
```

Below code is an example of creating a sort of list item like how a contact looks like
```xml
<RelativeLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent">

    <ImageView
        android:id="@+id/pebble"
        android:layout_width="56dp"
        android:layout_height="56dp"
        android:scaleType="centerCrop"
        android:src="@drawable/ocean" />

    <TextView
        android:id="@+id/pebble_beach_text"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="Pebble Beach"
        android:textAppearance="?android:textAppearanceMedium"
    	android:layout_toRightOf="@id/pebble" />

    <TextView
        android:id="@+id/california_text"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="California"
        android:textAppearance="?android:textAppearanceSmall"
        android:layout_toRightOf="@id/pebble"
        android:layout_below="@id/pebble_beach_text"/>
    

    <TextView
        android:id="@+id/miles_text"
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:text="10 miles away"
        android:textAppearance="?android:textAppearanceSmall" 
        android:layout_toRightOf="@id/pebble"
    	android:layout_below="@id/california_text"/>
</RelativeLayout>
```

### Padding and Margin:
Padding: 
* gets handled by TextView itself
* Potential code attributes are:
    * `android:padding="*dp"`
    * `android:paddingLeft="*dp"`
    * `android:paddingRight="*dp"`
    * `android:paddingTop="*dp"`
    * `android:paddingBottom="*dp"`
* default padding is 0dp so you can assign only top and bottom and the left and right will just be 0dp. 

Margin:
* managed by parent ViewGroup
* moves child *dps from the parent's edges. 
* can the dps can almost be seen as a force field because nothing can enter that specific dps of space
* Potential code attributes are:
    * `android:margin="*dp"`
    * `android:marginLeft="*dp"`
    * `android:marginRight="*dp"`
    * `android:marginTop="*dp"`
    * `android:marginBottom="*dp"`
* default margin works the same way as default padding

Example Code of Padding and Margin:
```xml
<LinearLayout
    xmlns:android="http://schemas.android.com/apk/res/android"
    android:layout_width="match_parent"
    android:layout_height="match_parent"
    android:background="#4f4f4f"
    android:orientation="vertical">

    <ImageView
        android:src="@drawable/ocean"
        android:layout_width="match_parent"
        android:layout_height="0dp"
        android:layout_weight="1"
        android:scaleType="centerCrop"/>

    <TextView
        android:text="You're invited!"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="@android:color/white"
        android:textSize="45sp"
        android:background="#009688"
        android:paddingTop="16dp"
        android:paddingLeft="16dp"
        android:paddingRight="16dp"
        android:paddingBottom="8dp" />

    <TextView
        android:text="Bonfire at the beach"
        android:layout_width="match_parent"
        android:layout_height="wrap_content"
        android:textColor="@android:color/white"
        android:textSize="24sp"
        android:background="#009688"
       	android:paddingLeft="16dp"
        android:paddingBottom="16dp"
        android:paddingRight="16dp"/>
</LinearLayout>
```

---

## Unsorted Notes:
 It is recommended that all touch targets be at least 48dp
 It is recommended to have alignment of
 * 8dp grid for components
* 4dp grid for type
* 4dp for iconography in toolbars

Special characers need to be escaped oddly in XML, I used [this website](https://www.advancedinstaller.com/user-guide/xml-escaped-chars.html) for reference

### Logging:
There are options for logging to Logcat. I'm not sure why they think this is important but the options are:
* e(String, String) (error)
* w(String, String) (warning)
* i(String, String) (information)
* d(String, String) (debug)
* v(String, String) (verbose)

All are listed [here](https://developer.android.com/reference/android/util/Log) as well

An example log is 
```java
Log.i("EnterpriseActivity.java", "Captain's Log, Stardate 43125.8. We have entered a spectacular binary star system in the Kavis Alpha sector on a most critical mission of astrophysical research.");
```
The first argument is the name of the class that the logging statement comes from. The second is the text you want to display.

### Accessing XML Resources from Java
 * [This](https://developer.android.com/guide/topics/resources/providing-resources#Accessing) is an article on what the `aapt` and the `R` class is.
 * It contains the resource IDs for all the resources in the `\res` directory
 * There is an `R` subclass for each type of resource and each of it's subclasses, for example, all of these exist for the R class: 
    * `R.drawable`, `R.string`, `R.layout`, `R.layout`, `R.color`, `R.id`
 * for each of the `R` subclasses there are corresponding XML codes:
    * `@drawable/photo`, `@string/<string name>`, `@layout/<layout file>`, `@id/<custom id>`, `@color/<specific color>`

An example function for an app that (when a button is pressed) will print everything on the screen looks like
```java
public void printToLogs(View view) {
   // Find first menu item TextView and print the text to the logs
   TextView textViewItem1 = (TextView) findViewById(R.id.menu_item_1);
   String menuItem1 = textViewItem1.getText().toString();
   Log.v("MainActivity", menuItem1);

   // Find second menu item TextView and print the text to the logs
   TextView textViewItem2 = (TextView) findViewById(R.id.menu_item_2);
   String menuItem2 = textViewItem2.getText().toString();
   Log.v("MainActivity", menuItem2);

   // Find third menu item TextView and print the text to the logs
   TextView textViewItem3 = (TextView) findViewById(R.id.menu_item_3);
   String menuItem3 = textViewItem3.getText().toString();
   Log.v("MainActivity", menuItem3);

   /*
    PSEUDOCODE
    1. Get the TextView with findViewById.
    2. Use the getter called getText to get the text. Another method called toString must be used to convert the result (which is a CharSequence) returned from getText into a String. It’s complicated, we know. Sometimes you will need to massage the data you can get from a getter into the right type and the only way to know is by reading the documentation.
    3. Then use the method Log.v to print the log. You could have used any of the Logging methods to print the log, we chose verbose.
   */
}
```
The total code, with the code (XML and Java both) can be found [here](https://gist.github.com/udacityandroid/0d6a67d340cdf28bbf6e).


---

## Notes From Happy Birthday App:

Remember that `wrap_content` refers to the content of the view itself while `match_parent` deals with the content of the parent. For example, if the `TextView` had the string "hi" assigned to it, wrap_content would make the width and height whatever the height and width of "hi" is. match_parent, however, will make sure the width and height match those of the parent view. It's pretty obvious but easily messed up. I totally did XD.

`layout_alignParent*` refers to the child's spot in the parent view. It will align itself with the parent. `layout_to*Of` has to do with the relative spots with siblings

`padding` will make the view larger, `layout_margin` only changes the space between edges and the view. Having both just adds the two values together. Like if `padding` = 20 and `layout_margin` = 20 then the box itself will be 40 dp away from whatever

### Her steps to go from a drawing to an app:
1. Select the views: what views will need to be there?
2. Position the views: where do the views need to be?
3. Style the views: once placed, add details and customization to it

--- 

## Notes from JustJava mk I

 Two cool things: Android Studio can reformat your code so the tabs and stuff are lined up (`code->Reformat Code`) and can rearrange your code for what it understands to be best practice and style with (`code->Rearrange Code`)

 Remember that `LinearLayout` doesn't accept the `android_layout_*` available for `RelativeLayout` isn't valid anymore. Instead of that you have to mess with `layout_weight` and the `android:orientation` attribute. Both below are valid

 ```xml
 android:orientation="vertical"
 <!--- OR -->
 android:orientation="horizontal"
 ```

as far as I can tell it appears that when Android Studio sees you using `LinearLayout` it autosets it to `android:orientation="horizontal"`

Instead of caps-locking the crap out of everything when coding, put `android:textAllCaps="T/F"` so it's easier to change if necessary

Always update the code on your android device to check the format actually still works

To update text on a `TextView` tag, use
```java
TextView <variable name> = (TextView) findViewByID(R.id.<id in xml>);

displayBView.setText(<variable name>);
```

When you're assigning an `onClick` method in the XML file, always remember that the method has to be public. I spent a good couple of minutes dealing with that 

---

## Notes From Basketball Scoring App
There are things called `android:layout_centerHorizontal` and `android:layout_centerVertical`. I'm still not certain what the `*_centerVertical` does, but the `*_centerHorizontal` places the object in middle of the right/left edges. (There's a word for it that I can't remember)

---

## Notes From JustJava mk II
You can put newline characters with `\n` still. Android Studio doesn't show them as a special character, but 
```xml 
<string name="price_text_view_string">Name: n/a \nQuantity: n/a \nTotal: n/a \nThank you!</string>
```
will spit out
```
Name: n/a
Quantity: n/a
Total: n/a
Thank you!
```
 when you view it as a string in a basic program

 There is a method called `onCreate()` which has a method called `setContentView(R.layout.\<layout_file\>);` which then goes through and sets up the current view based on the xml in the specific layout file specified

To set up a checkbox, use the `<CheckBox>` tag.

My S6 Edge+ had a funky red color when the checkbox was tagged. It's apparently changable with `android:buttonTint` attribute. Android Studio didn't like it because it's only supported over API 21. The internet said the way to fix that warning is to use something along the code below
```xml
<android.support.v7.widget.AppCompatCheckBox 
    android:layout_width="wrap_content" 
    android:layout_height="wrap_content" 
    app:buttonTint="@color/COLOR_HERE" /> 
```
The AppCompat* allows stuff to work with older api's but apparently this specific widged requires a specific widgit to allow it to run. 

To convert the checkbox data to a boolean, use
```java
boolean checked = ((CheckBox) view).isChecked();
```

Allow app to be scrollable using the `ScrollView` wrapper. You'd wrap whatver in the `ScrollView` tag like the code shown below:
```xml
<ScrollView
    android:layout_width="fill_parent"
    android:layout_height="wrap_content">
    <LinearLayout 
        android:layout_width="wrap_content"
        android:layout_height="wrap_content"
        android:orientation="vertical">
                  <!-- Content here -->
    </LinearLayout>
</ScrollView>
```

You can assign checkboxes to items too like the code below
```java
CheckBox check1 = (CheckBox) findViewById(R.id.check1);
CheckBox check2 = (CheckBox) findViewById(R.id.check2);

Boolean check1Selected = check1.isChecked();
Boolean check2Selected = check2.isChecked();
```

There is a text input view you can create with `<EditText>`. Example code below:
```xml
<EditText
    android:layout_width="match_parent"
    android:layout_height="wrap_content"
    android:hint="@string/name_string"
    android:inputType="textCapWords"/>
```

The `android:inputType="textCapWords` attribute will make each word after a space capitalized. It's good for entering names as you wouldn't have to manually capitalize every word

### Context

I had another class but I wanted to access the strings in another strings.xml folder. Like I had the `CoffeeOrderSummary.java` file but I wanted to access the strings in `InputScreen.java`. The way I found to do that was to pass a `Context` object. I'm still not certain what it is, tbh, but I know that you can pass it as `this`. An example would be 
```java
//in InputScreen.java
CoffeeOrderSummary cos = new CoffeeOrderSummary(..., this);

//in CoffeeOrderSummary.java
public class CoffeeOrderSummary
{
    private Context context
    CoffeeOrderSummary(..., Context pContext)
    {
        ...
        this.context = pContext;
    }

    public String summaryToString()
    {
        return context.getResources().getString(R.string.*) + ...;
    }
}
```
I've still got no idea why that works, but I get what the word "context" is, so I'm guessing it's like how `this` works, especially since it's apparently a `Context` object

### Localization:

This is for making sure that places with different languages or whatnot can use the app the same way that you could. There are a handful of stepps
* make sure your strings are hardcoded in the strings.xml folder
* use the `getResouces.getString(R.string.*)` method
* organize the strings for easier reference, leave comments too for both yourself and everyone else
* make sure to specify if there are strings you don't want translated by the translator (like names). Apparently you just use something called `xliff`? I'm trying to figure it out. Research it more later because I'm not getting it rn. A bunch of examples are found [here](https://gist.github.com/udacityandroid/759b8b4c9ed9e6806e90). A bunch of students from the Udacity course did work. It'll help in the future, I assume.
* actually provide the translations. Android Studio has a really nice feature where you can right-click on a strings.xml file and click on something similar to `Translations Editor` where you can put different values in and it'll make another .xml file based on the translations. It's all automatic, unfortunately I don't get it yet, soon though!

### Styles:

You can kind of "max define" a bunch of values with a Style. You put in it in the `main/res/values/styles.xml` file. An example is:
```xml
<?xml version="1.0" encoding="utf-8"?>
<resources>
    <style name="GreenText" parent="TextAppearance.AppCompat">
        <item name="android:textColor">#00FF00</item>
    </style>
</resources>
```
and then you can apply it with
```xml
<TextView
    style="@style/GreenText"
    ... />
```

This is like how you can add a day/night/blue/tan theme on apps. 

Note that styles.xml is in a resource folder. This means you can override the style and provide different attributes on differently sized devices. You just need to create a new styles.xml file, within the appropriate values folder. For example, if you wanted a HeaderTextStyle to show bigger text if the user is on a tablet with the smaller side’s screen width of 600 dp or greater, you could save a new version of the HeaderTextStyle in res/values-sw600dp/styles.xml folder. Check out this documentation for more information.

[Example](https://bitbucket.org/connor-prinster/android-development/raw/afd7bfa1ab9b75a7daeeb7bd6a5323a7075ec7e9/resources/styleExampleDefinitions.PNG) of what themes attributes look like

`android:colorAccent` refers to checkboxes and text fields

 ---

## Notes From Eating Cookies Program
to change the image in the ImageView object, use 
```java
ImageView imgView = (ImageView) findViewById(R.id.*);

imgView.setImageResource(R.drawable.*);
```

---

[comment]: #

## Links to helpful sites:

The current notes are from the Udacity course [Android Basics: User Interface](https://classroom.udacity.com/courses/ud834).

A [simple visualizer](https://labs.udacity.com/android-visualizer/) to check what code will look like on an android phone is.

The [official Android Documentation](https://developer.android.com/docs/). There are forums and everything.

[Google's best design practices](https://material.io/design/introduction/#principles) for things like motion, shapes, interaction, communication, iconography, color, navigation, layout, etc. It's super great. It seems like super well created design documentation.

A [styling blog](https://blog.stylingandroid.com/) for Android apps

A [support libraries blog](https://chris.banes.me/) for Android 

[Tips on debugging using Android Studio](https://developer.android.com/studio/debug/)

[Android development discussion podcast](http://fragmentedpodcast.com/)

[Android Weekly newsletter on Androi development](https://androidweekly.net/)

[A link to Daily Android](https://www.uplabs.com/android) which has resources for designers

[G+ Android Design Community](https://plus.google.com/communities/117140012142044995433)

Udacity recommended to search for "#AndroidDev" and "#Protip" for help so I [searched](https://www.google.com/search?ei=ybr9Wq-pJMfWjwS49b6oDg&q=%23androiddev+%23protip&oq=%23AndroidDev+&gs_l=psy-ab.1.0.0i20i263k1j0j0i30k1l2j0i10i30k1j0i30k1l2j0i10i30k1l2j0i30k1.11754.13789.0.15411.3.3.0.0.0.0.77.144.2.3.0....0...1c.1.64.psy-ab..0.3.251.6..35i39k1.108.mBV69085uGw) for those hashtags

[Hex Colorpicker](https://www.google.com/search?q=hex+color+picker&oq=hex+color+picker&aqs=chrome..69i57j0l5.2176j0j7&sourceid=chrome&ie=UTF-8) result on Google at least used to give you an easy to use color picker that gives many different versions of the same color

[Common Views Cheatsheet](https://bitbucket.org/connor-prinster/android-development/src/master/resources/common-android-views-cheat-sheet.pdf) for common android views. It's a binary file so you'll have to download it, but it's useful

[Intents Cheatsheet](https://developer.android.com/guide/components/intents-common?utm_source=udacity&utm_medium=course&utm_campaign=android_basics) for common intents

[Article on Localization-izing your app](https://developer.android.com/distribute/best-practices/launch/localization-checklist?utm_source=udacity&utm_medium=course&utm_campaign=android_basics#manage-strings) for language and what not

[Documentation](https://developer.android.com/guide/practices/screens_support?utm_source=udacity&utm_medium=course&utm_campaign=android_basics) for designing for different sizes of screens

[RecyclerView Documentation](https://developer.android.com/guide/topics/ui/layout/recyclerview#RecyclerView) on scrolling list of elements on large data sets

[Documentation](https://developer.android.com/guide/topics/ui/notifiers/notifications) on notifications

[Animation](https://developer.android.com/training/animation/) documentation

---
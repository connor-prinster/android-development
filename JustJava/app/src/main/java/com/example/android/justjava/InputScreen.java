package com.example.android.justjava;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class InputScreen extends AppCompatActivity
{
    /** contains the value of the number of coffees ordered */
    int numberOfCoffees = 0;
    /** holds the price of the standard coffee with no additions */
    double pricePerCoffee = 3.5;
    /** the boolean that will change based on if the whipped cream checkbox is on/off */
    boolean wantsWhippedCream = false;
    /** the boolean that will change based on if the chocolate checkbox is on/off */
    boolean wantsChocolate = false;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_input_screen);
    }

    /** composes the order.
     * @param view in order for the onClick method to work in the .xml file there must be a View object
     */
    public void generateOrderText(View view)
    {
        displayQuantity();
        CoffeeOrderSummary cos = new CoffeeOrderSummary(editName(), returnNumberOfCoffees(), calculatePrice(), checkboxResults(), this);
        composeEmail(cos.summaryToString());
    }

    /** edits the name on the order from the edit text tag */
    public String editName()
    {
        String name = getResources().getString(R.string.no_name_string);
        EditText nameField = (EditText) findViewById(R.id.name_field);
        if(nameField.getText().length() != 0)
        {
            name = nameField.getText().toString();
        }
        nameField.getText().clear();
        return name;
    }

    /** don't want to have access to numberOfCoffees so I made a function to retrieve it */
    private int returnNumberOfCoffees()
    {
        return numberOfCoffees;
    }

    /** calculates the price based on the checkbox values and then updates the price to be printed */
    private double calculatePrice()
    {
        double returnPrice = pricePerCoffee;
        if(wantsWhippedCream) returnPrice++;
        if(wantsChocolate) returnPrice++;

        return returnPrice * returnNumberOfCoffees();
    }

    /** updates the price and the string based on the checkbox values */
    public String checkboxResults()
    {
        String returnResult = getResources().getString(R.string.toppings_email_text) + getResources().getString(R.string.whipped_cream_email_text) + getResources().getString(R.string.no_string) + getResources().getString(R.string.chocolate_email_text) + getResources().getString(R.string.no_string);
        //String returnResult = "\nToppings: \n \tWhipped Cream: no \n\tChocolate: no";

        if(wantsWhippedCream || wantsChocolate)
        {
            returnResult = getResources().getString(R.string.toppings_email_text);
            returnResult += getResources().getString(R.string.whipped_cream_email_text);
            if(wantsWhippedCream)
            {
                returnResult += getResources().getString(R.string.yes_string);
            }
            else
            {
                //returnResult += " no\n";
                returnResult += getResources().getString(R.string.no_string);
            }
            returnResult += getResources().getString(R.string.chocolate_email_text);
            if(wantsChocolate)
            {
                returnResult += getResources().getString(R.string.yes_string);
            }
            else
            {
                returnResult += getResources().getString(R.string.no_string);
            }
        }
        return returnResult;
    }

    /**
     * sets the boolean values that should be set to the on/off of the checkboxes
     * @param view in order for the onClick method to work in the .xml file there must be a View object
     */
    public void setCheckboxValues(View view)
    {
        CheckBox whippedBox = (CheckBox) findViewById(R.id.whipped_cream_checkbox);
        CheckBox chocolateBox = (CheckBox) findViewById(R.id.chocolate_checkbox);

        wantsWhippedCream = whippedBox.isChecked();
        wantsChocolate = chocolateBox.isChecked();
    }

    /** increments the coffee count and then calls displayQuantity to print it
     * @param view in order for the onClick method to work in the .xml file there must be a View object
     */
    public void incrementCoffeeCount(View view)
    {
        numberOfCoffees++;
        displayQuantity();
    }

    /**
     * decrements the coffee count and then calls displayQuantity to print it
     * @param view in order for the onClick method to work in the .xml file there must be a View object
     */
    public void decrementCoffeeCount(View view)
    {
        //if there will be a negative number when the minus button is pressed, nothing happens
        if(returnNumberOfCoffees() > 0)
        {
            numberOfCoffees--;
        }
        displayQuantity();
    }

    /** updates the number of coffees on the screen */
    private void displayQuantity()
    {
        TextView quantityTextView = (TextView) findViewById(R.id.quantity_text_view);
        quantityTextView.setText(Integer.toString(numberOfCoffees));
    }

    /** takes the summary string being generated throughout this app and pushes it to an email app
     * @param summary takes the summary and then prints sends it to an email app
     */
    public void composeEmail(String summary)
    {
        Intent intent = new Intent(Intent.ACTION_SEND);
        intent.setType("*/*");
        intent.putExtra(Intent.EXTRA_SUBJECT, "Buy the Sinful Elixir");
        intent.putExtra(Intent.EXTRA_TEXT, summary);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
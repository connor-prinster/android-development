package com.example.android.justjava;

import android.content.Context;
import android.content.res.Resources;

import java.text.NumberFormat;

public class CoffeeOrderSummary
{
    /** contains the name of the person ordering */
    public String name;
    /** quantity of coffees ordered */
    private int quantity;
    /** price of the coffee */
    private double price;
    /** contains the fragment of the string regarding the checkboxes */
    private String checkBoxResults;

    private Context context;

    /**
     * creates the CoffeeOrderSummary object
     * @param pName the name passed to this object
     * @param pQuantity the number of coffees passed to this object
     * @param pPrice the price of the coffee ordered (including adjustments based on checkboxes
     * @param pCheckBoxResults the string that contains the checkbox string fragments
     */
    CoffeeOrderSummary(String pName, int pQuantity, double pPrice, String pCheckBoxResults, Context pContext)
    {
        this.name = pName;
        this.quantity = pQuantity;
        this.price = pPrice;
        this.checkBoxResults = pCheckBoxResults;
        this.context = pContext;
    }

    /**
     * the CoffeeOrderSummary object is turned into a string
     * @see String
     */
    public String summaryToString()
    {
        return context.getResources().getString(R.string.name_static_string) + name + context.getResources().getString(R.string.quantity_static_string) + quantity + context.getResources().getString(R.string.total_static_string) + NumberFormat.getCurrencyInstance().format(price) + checkBoxResults + context.getResources().getString(R.string.thank_you_static_string);
    }
}

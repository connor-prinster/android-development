package com.example.android.eatingcookies;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

public class eating_cookies extends AppCompatActivity {

    private int clickCounter = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eating_cookies);
    }

    public void changeValues(View view)
    {
        clickCounter++;
        ImageView imgView = (ImageView) findViewById(R.id.main_image);
        TextView buttonText = (TextView) findViewById(R.id.eat_button);
        if(clickCounter % 2 == 1)
        {
            buttonText.setText(R.string.full_string);
            imgView.setImageResource(R.drawable.after_cookie);
        }
        else
        {
            buttonText.setText(R.string.hungry_string);
            imgView.setImageResource(R.drawable.before_cookie);
        }

    }
}

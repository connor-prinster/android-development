package com.example.android.basketballscoretracker;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.text.NumberFormat;

public class scoreDisplay extends AppCompatActivity {

    int teamAScore = 0;
    int teamBScore = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_score_display);
    }

    public void aFreeThrow(View view)
    {
        teamAScore++;
        updateDisplays();
    }
    public void aTwoScore(View view)
    {
        teamAScore+=2;
        updateDisplays();
    }
    public void aThreeScore(View view)
    {
        teamAScore+=3;
        updateDisplays();
    }
    public void dropAScore(View view)
    {
        if(teamAScore > 0)
        {
            teamAScore--;
        }
        updateDisplays();
    }

    public void bFreeThrow(View view)
    {
        teamBScore++;
        updateDisplays();
    }
    public void bTwoScore(View view)
    {
        teamBScore+=2;
        updateDisplays();
    }
    public void bThreeScore(View view)
    {
        teamBScore+=3;
        updateDisplays();
    }
    public void dropBScore(View view)
    {
        if(teamBScore > 0)
        {
            teamBScore--;
        }
        updateDisplays();
    }

    public void resetScores(View view)
    {
        teamAScore = 0;
        teamBScore = 0;
        updateDisplays();
    }

    public void updateDisplays()
    {
        updateDisplayA();
        updateDisplayB();
    }

    private void updateDisplayA()
    {
        TextView displayAView = (TextView) findViewById(R.id.team_a_text);
        displayAView.setText(Integer.toString(teamAScore));
    }
    private void updateDisplayB()
    {
        TextView displayBView = (TextView) findViewById(R.id.team_b_text);
        displayBView.setText(Integer.toString(teamBScore));
    }
}
